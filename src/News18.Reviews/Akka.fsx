#r "../../packages/System.Collections.Immutable/lib/portable-net45+win8+wp8+wpa81/System.Collections.Immutable.dll"
#r "../../packages/Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"
#r "../../packages/Hyperion/lib/net45/Hyperion.dll"
#r "../../packages/FSPowerPack.Core.Community/Lib/Net40/FSharp.PowerPack.dll"
#r "../../packages/FSPowerPack.Linq.Community/Lib/Net40/FSharp.PowerPack.Linq.dll"
#r "../../packages/FsPickler/lib/net45/FsPickler.dll"
#r "../../packages/Akka/lib/net45/Akka.dll"
#r "../../packages/Akka.Serialization.Hyperion/lib/net45/Akka.Serialization.Hyperion.dll"
#r "../../packages/Reactive.Streams/lib/net45/Reactive.Streams.dll"
#r "../../packages/Akka.Streams/lib/net45/Akka.Streams.dll"
#r "../../packages/Akkling/lib/net45/Akkling.dll"
#r "../../packages/Akkling.Streams/lib/net45/Akkling.Streams.dll"

open Akka.Actor
open Akkling
open Akkling.Streams
open Akka.Streams
open Akka.Streams.Dsl

let system = System.create "rossmann" <| Configuration.parse("""akka {
                                                                    actor {
                                                                      serializers {
                                                                        hyperion = "Akka.Serialization.HyperionSerializer, Akka.Serialization.Hyperion"
                                                                      }
                                                                      serialization-bindings {
                                                                        "System.Object" = hyperion
                                                                      }
                                                                    }
                                                                    }""")

let materializer = system.Materializer()

let runnableGraph = Source.ofArray([|1;2;3;4;5;6;7;8;9;10|]) |> Graph.create1(fun (b)s -> 
                                                                                let graph = Graph.create(fun builder -> 
                                                                                                            let flow = builder.Add(Flow.id<int, int> 
                                                                                                                                    |> Flow.filter(fun x -> x > 2) 
                                                                                                                                    |> Flow.asyncMap(50)
                                                                                                                                                    (fun x -> async { 
                                                                                                                                                                       do! Async.Sleep(1000)
                                                                                                                                                                       return x * x 
                                                                                                                                                                    } ))
                                                                                                            let sink = builder.Add(Sink.forEach(fun x -> printfn "%A" x))
                                                                                                            builder.From(flow.Outlet).To(sink) |> ignore
                                                                                                            SinkShape(flow.Inlet)
                                                                                                        )
                                                                                                        
                                                                                let sink = b.Add(graph)                                                                                    
                                                                                b.From(s.Outlet).To(sink) |> ignore
                                                                                ClosedShape.Instance
                                                                             ) |> Graph.runnable |> Graph.run materializer