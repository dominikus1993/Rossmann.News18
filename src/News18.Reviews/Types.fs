namespace News18.Reviews
open System
open News18.Common.Types

type AppConfig = { virtualPath: string; physicalPath: string }

type Attachment =
    | Image of rossnetPath: string
    | Video of fileId: string * mediaLink: string

type InsertReviewAttachmentSuccess = { ReviewId: int
                                       LocalFilePath: string
                                       Sequence: int
                                       FileName: string
                                       Attachment: Attachment }   

type InsertReviewAttachmentError = { Reason: string
                                     ReviewId: int
                                     Sequence: int
                                     TypeId: int 
                                     LocalPath: string
                                     FileName: string
                                     MimeType: string }

[<CLIMutable>]
type DbReviewAttachmentError = { ReviewId: int
                                 Sequence: int
                                 TypeId: ReviewAttachmentType
                                 LocalPath: string
                                 Reason: string
                                 MimeType: string
                                 FileName: string
                                 CreationDate: DateTime;
                                 Attempt: int 
                                 Code: ErrorCode}
                                 
type InsertAttachment = { ReviewId: int
                          Sequence: int
                          TypeId: int
                          Url: string;
                          FileId: string
                          FileName: string }

type ReviewActorMessages = 
        | Reupload of DbReviewAttachmentError seq
        | Start
                                