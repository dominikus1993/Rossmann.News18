#r "../../packages/Dapper/lib/net451/Dapper.dll"
#r "../../packages/Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"
#load "Types.fs"
#load "Database.fs"

open System.Data.SqlClient
open News18.Reviews
open System 
open Newtonsoft.Json

[<Literal>]
let RossnetConnectionString = "Data Source=devrossap1v;Initial Catalog=dbrossnetpl;User ID=devrossnet;Password=12345678"

let db = Database.db RossnetConnectionString
match db.GetUploadAttachmentsErrrosAsync() |> Async.RunSynchronously with
| Ok data ->    
    printfn "%A" data
| Error err ->
    printfn "%A" err

let (_, date) = DateTime.TryParse("2018-02-08 23:59:59.900")
let b = JsonConvert.SerializeObject(date)

