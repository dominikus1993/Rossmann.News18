namespace News18.Reviews
open System.Data.Common
open Dapper
open System.Data
open News18.Common.Types

module Database = 
    open System.Data.SqlClient
    
    type IReviewRepository = 
        abstract member GetUploadAttachmentsErrrosAsync: unit -> Async<Result<DbReviewAttachmentError seq, exn>>
        abstract member InsertReviewAttachmentSuccess: req: InsertReviewAttachmentSuccess -> Async<unit>
        abstract member InsertReviewAttachmentError: req: InsertReviewAttachmentError -> Async<unit>
        abstract member DeleteReviewAttachmentError: reviewId: int * sequence: int -> Async<unit>
        
    let private getUploadAttachmentsErrrosAsync (connection:#DbConnection) = 
        async {
            try
                let! res = connection.QueryAsync<DbReviewAttachmentError>("[News18].[GetUploadAttachmentsErrros]", commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask
                return Ok res
            with
            | ex -> return Error ex
        }
        
    let private insertAttachment (connection:#DbConnection) (req: InsertReviewAttachmentSuccess) =
        async {
            try
                let struct (mediaType, url, fileId) = 
                    match req.Attachment with
                    | Image url ->
                        struct (ReviewAttachmentType.Image |> int, url, null)
                    | Video(fileId, mediaLink) ->
                        struct  (ReviewAttachmentType.Video |> int, mediaLink, fileId)
                let args: InsertAttachment = { ReviewId = req.ReviewId; Sequence = req.Sequence; TypeId = mediaType; Url = url; FileId = fileId; FileName = req.FileName }
                do! connection.ExecuteAsync("[News18].[InsertReviewAttachment]", args , commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask |> Async.Ignore
                return Ok(())
            with
            | exn -> 
                return Error(exn)
        }

    let private insertReviewAttachmentError (connection:#DbConnection) (args: InsertReviewAttachmentError) =
        async {
            try
                do! connection.ExecuteAsync("[News18].[InsertReviewAttachmentUploadError]", args , commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask |> Async.Ignore
                return Ok(())
            with
            | exn -> 
                return Error(exn)
        }
        
    let private deleteReviewAttachmentError (connection:#DbConnection)(reviewId: int, sequence: int) =
        async {
            try
                let args = dict ["ReviewId", reviewId; "Sequence", sequence]
                do! connection.ExecuteAsync("[News18].[RemoveReviewAttachmentUploadError]", args , commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask |> Async.Ignore
                return Ok(())
            with
            | exn -> 
                return Error(exn)
        }
    
     //https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/object-expressions
    let db connectionString = 
        { new IReviewRepository with
            member ___.GetUploadAttachmentsErrrosAsync() =
                async {
                    use connection = new SqlConnection(connectionString)
                    return! getUploadAttachmentsErrrosAsync(connection)
                }
            member ___.InsertReviewAttachmentSuccess(req) =
                async {
                    use connection = new SqlConnection(connectionString)
                    do! insertAttachment connection req |> Async.Ignore
                    return ()
                }
            member ___.InsertReviewAttachmentError(req) =
                async {
                    use connection = new SqlConnection(connectionString)
                    do! insertReviewAttachmentError connection req |> Async.Ignore
                    return ()
                }
            member ___.DeleteReviewAttachmentError(reviewId: int, sequence: int) =
                async {
                    use connection = new SqlConnection(connectionString)
                    do! deleteReviewAttachmentError connection (reviewId, sequence) |> Async.Ignore
                    return ()
                }                
        }

