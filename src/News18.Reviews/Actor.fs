namespace News18.Reviews
open Akkling.Streams
open System.IO
open System
open Google.Apis.Drive.v3
open Google.Apis.Drive.v3.Data
open Akka.Streams
open News18.Common.Types
open Akkling

module Actor =
    open Akka.Actor
    open Akka.Streams.Dsl
    open Akkling.Streams.Operators
    open Database

    let uploadImageAttachment(physicalPath: string)(virtualPath: string) = 
        Flow.id
        |> Flow.filter(fun (x:DbReviewAttachmentError) -> x.TypeId = ReviewAttachmentType.Image)
        |> Flow.log("reupload image attachment")
        |> Flow.attrs(Attributes.CreateLogLevels(Akka.Event.LogLevel.InfoLevel))
        |> Flow.map(fun x ->
                        try
                            let newFileName = sprintf "%s%s" (Guid.NewGuid().ToString()) (Path.GetExtension(x.FileName.Replace("""\""", """\\""")))
                            let newPath = Path.Combine(physicalPath.Replace("""\""", """\\"""), Path.GetFileName(newFileName))
                            let fileByteData = File.ReadAllBytes(x.LocalPath.Replace("""\""", """\\"""))
                            File.WriteAllBytes(newPath, fileByteData)
                            Ok({ ReviewId = x.ReviewId
                                 LocalFilePath = x.LocalPath
                                 Sequence = x.Sequence
                                 FileName = newFileName
                                 Attachment = Image(sprintf "%s/%s" virtualPath newFileName) } )
                        with
                        | exn -> 
                            Error({ Reason = sprintf "%s | %s" exn.Message exn.StackTrace
                                    ReviewId = x.ReviewId
                                    Sequence = x.Sequence
                                    TypeId = x.TypeId |> int
                                    LocalPath = x.LocalPath
                                    FileName = x.LocalPath
                                    MimeType = x.MimeType })
                    )    

    let uploadVideoAttachment(service: DriveService) = 
        Flow.id
        |> Flow.filter(fun (x: DbReviewAttachmentError) -> x.TypeId = ReviewAttachmentType.Video)
        |> Flow.log("reupload video attachment")
        |> Flow.attrs(Attributes.CreateLogLevels(Akka.Event.LogLevel.InfoLevel))
        |> Flow.asyncMapUnordered (1000) (fun x ->
                        async {
                            try
                                let newFileName = sprintf "%s%s" (Guid.NewGuid().ToString()) (Path.GetExtension(x.FileName.Replace("""\""", """\\""")))
                                let meta = Google.Apis.Drive.v3.Data.File(Name = Path.GetFileName(newFileName))
                                use stream = new MemoryStream(File.ReadAllBytes(x.LocalPath.Replace("""\""", """\\""")))
                                let req: FilesResource.CreateMediaUpload = service.Files.Create(meta, stream, x.MimeType)
                                req.Fields <- "id, webContentLink"

                                let! uploadResult = req.UploadAsync() |> Async.AwaitTask
                                let! perm = service.Permissions.Create(Permission(Role = "reader", Type = "anyone"), req.ResponseBody.Id).ExecuteAsync() |> Async.AwaitTask

                                return Ok({ ReviewId = x.ReviewId
                                            LocalFilePath = x.LocalPath
                                            Sequence = x.Sequence
                                            FileName = newFileName
                                            Attachment = Video(req.ResponseBody.Id, req.ResponseBody.WebContentLink) } )
                            with
                            | exn -> 
                                return Error({ Reason = sprintf "%s | %s" exn.Message exn.StackTrace
                                               ReviewId = x.ReviewId
                                               Sequence = x.Sequence
                                               TypeId = x.TypeId |> int
                                               LocalPath = x.LocalPath
                                               FileName = x.LocalPath
                                               MimeType = x.MimeType })
                        }
                    )

    let uploadImageResultFlow (repository: IReviewRepository) =
            Flow.asyncMap(50)(fun x -> 
                                async {
                                    match x with
                                    | Error err -> 
                                        do! repository.InsertReviewAttachmentError(err)
                                        return x
                                    | Ok success -> 
                                        do! repository.InsertReviewAttachmentSuccess(success)
                                        return x
                                }
                             )

    let deleteReviewAttachmentError(repository: IReviewRepository) =
        Flow.asyncMap(50)(fun (x: Result<InsertReviewAttachmentSuccess, InsertReviewAttachmentError>) -> 
                            async {
                                match x with
                                | Ok success -> 
                                    do! repository.DeleteReviewAttachmentError(success.ReviewId, success.Sequence)
                                    return Ok success
                                | Error err -> 
                                    return Error err
                            }
                         )

    let reuploadImagesFlowGraph(repository: IReviewRepository)(service: DriveService)(config: AppConfig) =  
        Graph.create(fun builder -> 
                        let inBroadcast = Broadcast(2) |> builder.Add
                        let imgFlow = uploadImageAttachment(config.physicalPath)(config.virtualPath) |> Flow.async |> builder.Add
                        let videoFlow = uploadVideoAttachment(service) |> Flow.async |> builder.Add
                        let resultMerge = Merge<Result<InsertReviewAttachmentSuccess, InsertReviewAttachmentError>>(2) |> builder.Add          
                        let resultFlow = Flow.id |> uploadImageResultFlow(repository) |> builder.Add
                        let deleteReviewAttachmentErrorFlow = Flow.id |> deleteReviewAttachmentError(repository) |> builder.Add

                        builder.From(inBroadcast.Out(0)).Via(imgFlow).To(resultMerge.In(0)) |> ignore
                        builder.From(inBroadcast.Out(1)).Via(videoFlow).To(resultMerge.In(1)) |> ignore
                        builder.From(resultMerge.Out) => resultFlow => deleteReviewAttachmentErrorFlow |> ignore
                        FlowShape(inBroadcast.In, deleteReviewAttachmentErrorFlow.Outlet)
                    )

    let reviewId(repository: IReviewRepository)(service: DriveService)(config: AppConfig)(mailbox: Actor<ReviewActorMessages>) = 
        let rec loop() =
            actor {
                let! msg = mailbox.Receive()
                match msg with
                | Start -> 
                    pipeTo 
                        (mailbox.Sender() |> untyped)
                        (mailbox.Self)
                        (async {
                            let! result = repository.GetUploadAttachmentsErrrosAsync()
                            match result with
                            | Ok data ->
                                return Reupload data
                            | Error err ->
                                return failwith err.Message
                            })
                    return loop()
                | Reupload workplaceErrors -> 
                    use materializer = mailbox.System.Materializer()
                    let flow = reuploadImagesFlowGraph(repository)(service)(config)
                    let deleteAttachmentFlow = Flow.id
                                                    |> Flow.map(fun (res: Result<InsertReviewAttachmentSuccess, InsertReviewAttachmentError>) ->
                                                                match res with
                                                                | Ok success ->
                                                                    File.Delete(success.LocalFilePath)
                                                                    Ok success
                                                                | Error err ->
                                                                    Error err
                                                            )
                                                                
                    let! result = Source.ofSeq(workplaceErrors) 
                                    |> Source.filter(fun x -> File.Exists(x.LocalPath)) 
                                    |> Source.via flow
                                    |> Source.via deleteAttachmentFlow
                                    |> Source.runWith materializer (Sink.fold([]) (fun acc x -> x :: acc))

                    mailbox.Sender() <! result
                    return loop()
            }
        loop()