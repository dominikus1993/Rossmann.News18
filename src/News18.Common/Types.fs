namespace News18.Common.Types

type ReviewAttachmentType = 
    | Video = 0
    | Image = 1

type ErrorCode = 
    | Unknown = -1
    | UploadStarted = 1
    | ImageDestinationNotAvailable = 2
    | VideoDestinationNotAvailable = 3
    | NotWorkplaceUser = 4
    | InvalidWorkplaceAccessToken = 5
    | CantUploadSuccess = 6
    | BadRequest = 400
    | Unauthorized = 401
    | PayloadTooLarge = 413
    | DestinationServerError = 500