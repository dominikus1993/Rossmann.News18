#r "../../packages/Dapper/lib/net451/Dapper.dll"
#r "../../packages/Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"
#load "../News18.Common/Types.fs"
#load "Types.fs"
#load "Database.fs"
open System.Data.SqlClient
open News18.Workplace
open News18.Workplace.Dto
open System 
open Newtonsoft.Json

[<Literal>]
let RossnetConnectionString = "Data Source=devrossap1v;Initial Catalog=dbrossnetpl;User ID=devrossnet;Password=12345678"

let repo = Database.db RossnetConnectionString

// async {
//     let! r = repo.GetUploadSocialMediaAttachmentsErrrosAsync()
//     let rr = r |> Result.map(fun x -> x |> Seq.map(fun y -> repo.GetReviewData y.ReviewId))
//     match rr with
//     | Ok w -> 
//         let! result = Async.Parallel(w)
//         printfn "%A" result
//     | Error err ->
//         printfn "%A" err
// } |> Async.RunSynchronously


type FbMedia(id: string) =
    [<JsonProperty("media_fbid")>]
    member val Id = id

let media = FbMedia("77")

JsonConvert.SerializeObject(media)