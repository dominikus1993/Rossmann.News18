namespace News18.Workplace
open System.Data.Common
open Dapper
open System.Data
open System.Data.SqlClient
open Dto

module Database = 

    type IWorkplaceRepository = 
        abstract member GetUploadSocialMediaAttachmentsErrrosAsync: unit -> Async<Result<WorkplaceError seq, exn>>
        abstract member GetWorkplaceUserAndGroupId: userId: int -> Async<Result<WorkplaceUserAndGroupId, string>>
        abstract member GetReviewData: reviewId: int -> Async<Result<WorkplaceReviewData, string>>
        abstract member InsertWorkplaceAttachment: attachment: InsertWorkplaceAttachment -> Async<Result<unit, string>>

    let private validateSingleResult data =
        if data |> Seq.isEmpty then
            Error "No Data"
        else 
            Ok (data |> Seq.head) 

    let private getWorkplaceErrors(connection:#DbConnection)  = 
        async {
            try
                let! res = connection.QueryAsync<WorkplaceError>("[News18].[GetUploadSocialMediaAttachmentsErrros]", commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask
                return Ok res
            with
            | ex -> return Error ex
        }

    let private getWorkplaceUserAndGroupId(connection:#DbConnection)(userId: int) =
        async {
            try
                let args = DynamicParameters()
                args.Add("@UserId", userId)
                let! result = connection.QueryAsync<WorkplaceUserAndGroupId>("[Workplace].[GetWorkplaceUserId]", args, commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask
                return result |> validateSingleResult
            with 
            | ex -> return Error (ex.ToString())  
        }

    let private getReviewData(connection:#DbConnection)(reviewId: int) =
        async {
            try
                let args = DynamicParameters()
                args.Add("@ReviewId", reviewId)  
                let! result = connection.QueryAsync<WorkplaceReviewData>("[News18].[GetReviewData]", args, commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask
                return result |> validateSingleResult
            with 
            | ex -> return Error (ex.ToString())          
        }

    let private insertWorkplaceAttachment(connection:#DbConnection)(attachment: InsertWorkplaceAttachment)  =
        async {
            try
                do! connection.ExecuteAsync("[News18].[InsertReviewSocialMediaAttachment]", attachment, commandType = System.Nullable(CommandType.StoredProcedure)) |> Async.AwaitTask |> Async.Ignore
                return Ok()
            with 
            | ex -> 
                return Error (ex.ToString())          
        } 

    let db connectionString = 
        { new IWorkplaceRepository with
            member ___.GetUploadSocialMediaAttachmentsErrrosAsync() =
                async {
                    use connection = new SqlConnection(connectionString)
                    return! getWorkplaceErrors connection
                }   
            member ___.GetWorkplaceUserAndGroupId(userId) =
                async {
                    use connection = new SqlConnection(connectionString)
                    return! getWorkplaceUserAndGroupId connection userId
                }
            member ___.GetReviewData(reviewid) =
                async {
                    use connection = new SqlConnection(connectionString)
                    return! getReviewData connection reviewid
                }
            member ___.InsertWorkplaceAttachment(attachment) =
                async {
                    use connection = new SqlConnection(connectionString)
                    return! insertWorkplaceAttachment connection attachment                    
                }             
        }