namespace News18.Workplace
open Database
open Akkling.Streams
open Akkling
open Models
open Newtonsoft.Json
open Dto
open Akka.Streams
open Messages
open System.IO
open Akka.Event
open News18.Common.Types
open Hopac
open HttpFs.Client
open System.Linq
open System
open Google.Apis.Drive.v3
open System.Diagnostics
open System.Xml.Linq
open System.IO
open System.IO

module Actor = 

    let reuploadImage(message: string)(groupId: int64)(portalDomain: string, fileUrl: string)(wokrplaceUserToken: string) =
        job {
            let uri = sprintf "https://graph.facebook.com/%d/photos" groupId
            let fileContentype = match ContentType.parse(System.Web.MimeMapping.GetMimeMapping(fileUrl)) with | Some x -> x | None -> ContentType.create("image", "*")
            use! resp = Request.createUrl Get (sprintf "%s/%s" portalDomain fileUrl)  |> getResponse
            use memoryStream = new MemoryStream()
            do! resp.body.CopyToAsync memoryStream |> Job.awaitUnitTask
            let! response = Request.createUrl Post (uri)
                                    |> Request.setHeader (ContentType (ContentType.parse "application/x-www-form-urlencoded" |> Option.get))
                                    |> Request.setHeader (Authorization (sprintf "Bearer %s" wokrplaceUserToken))
                                    |> Request.body(BodyForm 
                                            [
                                               NameValue("caption", message)
                                               NameValue("published", "false")
                                               FormFile("source", (fileUrl, fileContentype, Binary (memoryStream.ToArray())))
                                            ])
                                    |> Request.responseAsString
            return response |> JsonConvert.DeserializeObject<WorkplaceUploadAttachmentResponse>
        }

    let reuploadImages (w: Result<UploadWorkplaceMedia, string>) =
        job {
            match w with
            | Ok data ->
                match data with 
                | { WorkplaceMedia = Some(Images(urls, portalDomain)); Message = Some message; GroupId = Some groupId; UserWorkplaceAccessToken = Some accessToken } ->
                    let! images = urls |> Seq.map(fun url -> reuploadImage message groupId (portalDomain, url) accessToken) |> Job.seqCollect
                    let mediaImagesJson = images.Where(fun x -> String.IsNullOrEmpty(x.Id) |> not).Select(fun x -> FbMedia(x.Id)) |> JsonConvert.SerializeObject
                    let uri = sprintf "https://graph.facebook.com/%d/feed" groupId
                    let! response = Request.createUrl Post (uri)
                                        |> Request.setHeader (ContentType (ContentType.parse "application/x-www-form-urlencoded" |> Option.get))
                                        |> Request.setHeader (Authorization (sprintf "Bearer %s" accessToken))
                                        |> Request.body(BodyForm 
                                                [
                                                   NameValue("message", message)
                                                   NameValue("published", "false")
                                                   NameValue("attached_media", mediaImagesJson)
                                                ])
                                        |> Request.responseAsString
                    return Ok { data with WorkplaceResponse = Some (response |> JsonConvert.DeserializeObject<WorkplaceUploadAttachmentResponse>) }
                | _ -> return Result.Error "no error"   
            | _ -> return Result.Error "no error"       

        } |> Job.toAsync
        
    let downloadVideo(config: WorkplaceActorConfig)(w: Result<UploadWorkplaceMedia, string>) =
        async {
            match w with
            | Ok data ->
                match data with
                | { WorkplaceMedia = Some(Video(fileId, fileName, _)) } -> 
                    let req = config.DriveService.Files.Get(fileId) 
                    let tmpPath = sprintf "%s\\%s" config.TmpVideoPath fileName
                    use stream = new FileStream(tmpPath, FileMode.Create)
                    do! req.DownloadAsync(stream) |> Async.AwaitTask |> Async.Ignore
                    return Ok { data with WorkplaceMedia = Some(Video(fileId, fileName, Some tmpPath)) }
                | _ -> 
                    return Result.Error "Can't save video at tmp path"
            | Error err -> 
                return Result.Error err
        }

    let convertVideo (ffmpegPath: string)(w: Async<Result<UploadWorkplaceMedia, string>>) =
        async {
            let! result = w 
            match result with
            | Ok data ->
                match data with
                | { WorkplaceMedia = Some(Video(fileId, fileName, Some path)) } ->
                    let scaledVideoPath = path.Replace(path |> Path.GetFileNameWithoutExtension, Guid.NewGuid().ToString())
                    let arguments = sprintf "-i %s -vf scale=1280:720 -hide_banner %s" path scaledVideoPath
                    let start = ProcessStartInfo(Arguments = arguments, FileName = ffmpegPath, WindowStyle = ProcessWindowStyle.Hidden, CreateNoWindow = false, UseShellExecute = false)
                    use exe = Process.Start(start)
                    exe.WaitForExit()
                    File.Delete(path)
                    return Ok { data with WorkplaceMedia = Some(Video(fileId, fileName, Some scaledVideoPath)) }
                | _ -> 
                    return Result.Error "Can't use FFmpeg"
            | Error err -> 
                return Result.Error err
        }

    let reuploadVideo(w: Async<Result<UploadWorkplaceMedia, string>>) = 
        job {
            let! result = w
            match result with
            | Ok data ->
                match data with 
                | { WorkplaceMedia = Some(Video(_, _, Some filePath)); Message = Some message; Title = Some title; GroupId = Some groupId; UserWorkplaceAccessToken = Some accessToken } -> 
                    let uri = sprintf "https://graph.facebook.com/%d/videos" groupId
                    let fileContentype = match ContentType.parse(System.Web.MimeMapping.GetMimeMapping(filePath)) with | Some x -> x | None -> ContentType.create("video", "*")
                    let! response = Request.createUrl Post (uri)
                                        |> Request.setHeader (ContentType (ContentType.parse "application/x-www-form-urlencoded" |> Option.get))
                                        |> Request.setHeader (Authorization (sprintf "Bearer %s" accessToken))
                                        |> Request.body(BodyForm 
                                                [
                                                   NameValue("title", title)
                                                   NameValue("description", message)
                                                   NameValue("no_story", "true")
                                                   FormFile("source", (filePath, fileContentype, Binary (File.ReadAllBytes filePath)))
                                                ])
                                        |> Request.responseAsString                   
                    return Ok { data with WorkplaceResponse = Some (response |> JsonConvert.DeserializeObject<WorkplaceUploadAttachmentResponse>) }
                | _ -> return Result.Error "no error"  
            | _ -> return Result.Error "no error"       
        } |> Job.toAsync 

    let deleteVideo (w: Async<Result<UploadWorkplaceMedia, string>>)  =
        async {
            let! result = w
            match result with 
            | Ok { WorkplaceMedia = Some(Video(_, _, Some filePath)) } -> 
                File.Delete(filePath)
                return result
            | _ -> 
                return result            
        }

                               
    let chooseSolution(config: WorkplaceActorConfig)(w: Result<UploadWorkplaceMedia, string>) =
        match w with 
        | Ok { WorkplaceMedia = Some(Images(_)); } ->
            w |> reuploadImages
        | Ok { WorkplaceMedia = Some(Video(_)); ErrorCode = ErrorCode.PayloadTooLarge } ->
            w |> downloadVideo (config) |> convertVideo(config.FFmpegPath) |> reuploadVideo |> deleteVideo
        | Ok { WorkplaceMedia = Some(Video(_)) } -> 
            w |> downloadVideo (config) |> reuploadVideo |> deleteVideo
        | _ -> 
            async { return Result.Error "No Media Data" } 

    let getWorkplaceUserAndGroupId (repo: IWorkplaceRepository) =
        Flow.id
            |> Flow.asyncMap(50)
                (fun (workplace: UploadWorkplaceMedia) ->
                    async {
                        match workplace.WorkplaceUserId, workplace.GroupId with
                        | (None, None) -> 
                            let! workplaceGroupId = repo.GetWorkplaceUserAndGroupId(workplace.ReviewerId)
                            return workplaceGroupId 
                                    |> Result.map(fun w -> { workplace with GroupId = Some w.GroupId; WorkplaceUserId = Some w.WorkplaceUserId })
                        | _ -> 
                            return Ok workplace
                    } 
                )

    let getUserWorkplaceAccessToken workplaceAccessToken = 
        Flow.id
            |> Flow.map
                        (fun (workplace: Result<UploadWorkplaceMedia, string>) -> 
                            match workplace with
                            | Ok data -> 
                                match data with
                                | { WorkplaceUserId = Some userId } ->
                                    let uri = sprintf "https://graph.facebook.com/%d?fields=impersonate_token" userId
                                    let result = Request.createUrl Get (uri)
                                                   |> Request.setHeader (ContentType (ContentType.parse "application/json" |> Option.get))
                                                   |> Request.setHeader (Authorization (sprintf "Bearer %s" workplaceAccessToken))
                                                   |> Request.responseAsString
                                                   |> Hopac.Hopac.run
                                                   |> JsonConvert.DeserializeObject<GetWorkplaceAccessTokenResponse>
                                    Ok { data with UserWorkplaceAccessToken = Some result.Token}
                                | _ -> Result.Error "Error"
                            | Error err -> Result.Error err
                        )
            |> Flow.async

    let getReviewData(repo: IWorkplaceRepository)(portalDomain: string) = 
        Flow.id
            |> Flow.asyncMap
                            (50)
                            (fun (workplace: Result<UploadWorkplaceMedia, string>) -> 
                                async {
                                    match workplace with
                                    | Ok data ->
                                        let! result = repo.GetReviewData data.ReviewId
                                        return result
                                                 |> Result.map(fun r -> 
                                                                    let jsonData = JsonConvert.DeserializeObject<WorkplaceAttachment array>(r.AttachmentsJson)
                                                                    let attachments = if jsonData |> Array.exists(fun x -> x.TypeId = ReviewAttachmentType.Video) then
                                                                                        let (fileId, fileName) = jsonData |> Array.filter(fun x -> x.TypeId = ReviewAttachmentType.Video) |> Array.map(fun x -> x.FileId, x.FileName) |> Array.head 
                                                                                        Video (fileId, fileName, None) 
                                                                                      else 
                                                                                        Images (jsonData |> Array.filter(fun x -> x.TypeId = ReviewAttachmentType.Image) |> Array.map(fun x -> (x.FileUrl)), portalDomain)
                                                                    { data with Title = Some r.Title; Message = Some r.Message; WorkplaceMedia = Some attachments }
                                                               )
                                    | Error err -> 
                                        return Result.Error err
                                })
    
    let tryFixWorkplaceProblemFlow(repo: IWorkplaceRepository) (config: WorkplaceActorConfig) =
        Flow.id
            |> Flow.asyncMap(50)
                            (fun (workplace: Result<UploadWorkplaceMedia, string>) ->
                                async {
                                    let! result = (workplace |> chooseSolution(config))
                                    match result with
                                    | Ok { ReviewId = reviewId; GroupId = Some groupId; WorkplaceMedia = Some(media); WorkplaceResponse = Some response  } -> 
                                        let! res = repo.InsertWorkplaceAttachment({ ReviewId = reviewId; GroupId = groupId; TypeId = (match media with | Video(_) -> ReviewAttachmentType.Video |> int | _ -> ReviewAttachmentType.Image |> int) ;  WorkplaceUserId = response.Id })
                                        return res
                                    | Error err -> 
                                        return Result.Error err
                                    | _ -> 
                                        return Result.Error "Unknown Error"
                                } 
                            )

    let getWorkplaceReviewData (repo: IWorkplaceRepository) (config: WorkplaceActorConfig) = 
        Graph.create(fun builder ->
                        let accessTokenFlow = Flow.id 
                                                |> Flow.via (getWorkplaceUserAndGroupId repo) 
                                                |> Flow.via (getUserWorkplaceAccessToken config.WorkplaceBotAccessToken) 
                                                |> Flow.via (getReviewData repo config.PortalDomain)
                                                |> builder.Add
                        FlowShape(accessTokenFlow.Inlet, accessTokenFlow.Outlet)
                    )

    let workplaceActor(repository: IWorkplaceRepository)(config: WorkplaceActorConfig)(mailbox: Actor<WorkplaceActorMessage>) = 
        let rec loop() =
            actor {
                let! msg = mailbox.Receive()
                match msg with
                | Start -> 
                    pipeTo 
                        (mailbox.Sender() |> untyped)
                        (mailbox.Self)
                        (async {
                            let! result = repository.GetUploadSocialMediaAttachmentsErrrosAsync()
                            match result with
                            | Ok data ->
                                return Reupload data
                            | Error err ->
                                return failwith err.Message
                            })
                    return loop()
                | Reupload workplaceErrors -> 
                    let flow = getWorkplaceReviewData(repository)(config)
                    let logFLow = Flow.id 
                                    |> Flow.logf "Workplace Log" (fun (res: Result<UploadWorkplaceMedia, string>) -> (sprintf "Result %A" res) :> obj)
                                    |> Flow.attrs(Attributes.CreateLogLevels LogLevel.InfoLevel)
                    let resultLogFlow = Flow.id 
                                            |> Flow.logf "Workplace Result Log" (fun (res: Result<unit, string>) -> (sprintf "Result %A" res) :> obj)
                                            |> Flow.attrs(Attributes.CreateLogLevels LogLevel.InfoLevel)                                    
                    let! result = Source.ofSeq(workplaceErrors) 
                                            |> Source.map(fun we -> { ReviewId = we.ReviewId; ReviewerId = we.ReviewerId; ErrorCode = we.ErrorCode; GroupId = None; WorkplaceUserId = None; UserWorkplaceAccessToken = None; Title = None; Message = None; WorkplaceMedia = None; WorkplaceResponse = None })
                                            |> Source.via flow
                                            |> Source.via logFLow
                                            |> Source.via (tryFixWorkplaceProblemFlow(repository)(config))
                                            |> Source.via resultLogFlow
                                            |> Source.runWith (mailbox.System.Materializer()) (Sink.fold([]) (fun acc x -> x :: acc))
                    mailbox.Sender() <! result
                    return loop()
            }
        loop()