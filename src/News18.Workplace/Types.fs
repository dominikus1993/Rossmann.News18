namespace News18.Workplace
open System
open Google.Apis.Drive.v3

module Dto =
    open Newtonsoft.Json      
    open News18.Common.Types

    [<CLIMutable>]
    type WorkplaceError = { Id: int
                            ReviewId: int
                            ReviewerId: int
                            GroupId: Nullable<int64>
                            WorkplaceUserId: Nullable<int64> 
                            ErrorCode: ErrorCode }

    [<CLIMutable>]
    type WorkplaceUserAndGroupId = { WorkplaceUserId: int64; GroupId: int64 }

    [<CLIMutable>]
    type GetWorkplaceAccessTokenResponse = { [<JsonProperty("impersonate_token")>] Token: string; [<JsonProperty("id")>]  Id: string }
    
    [<CLIMutable>]
    type WorkplaceReviewData = { Title: string; Message: string; AttachmentsJson: string }

    [<CLIMutable>]
    type WorkplaceAttachment = { FileId: string; FileUrl: string; FileName: string; TypeId: ReviewAttachmentType }

    type WorkplaceActorConfig = { PortalDomain: string; WorkplaceBotAccessToken: string; TmpVideoPath: string; FFmpegPath: string; DriveService: DriveService }

    [<CLIMutable>]
    type WorkplaceUploadAttachmentResponse = { [<JsonProperty("id")>] Id: string }

    type InsertWorkplaceAttachment = { ReviewId: int; GroupId: int64; TypeId: int;  WorkplaceUserId: string }

    type FbMedia(id: string) =
        [<JsonProperty("media_fbid")>]
        member val Id = id

open Dto

module Models =
    open News18.Common.Types
    
    type WorkplaceMedia = 
        | Images of fileNames: string seq * portalDomain: string
        | Video  of fileId: string * fileName: string * path: string option

    type UploadWorkplaceMedia = { ReviewId: int
                                  ReviewerId: int
                                  ErrorCode: ErrorCode 
                                  GroupId: int64 option
                                  WorkplaceUserId: int64 option
                                  UserWorkplaceAccessToken: string option
                                  Title: string option
                                  Message: string option
                                  WorkplaceMedia: WorkplaceMedia option
                                  WorkplaceResponse: WorkplaceUploadAttachmentResponse option }
                                 
    type UploadWorkplaceAttachmentResult = { ReviewId: int; GroupId: int64; Response: WorkplaceUploadAttachmentResponse; MediaType: ReviewAttachmentType;  }
module Messages =
    open Dto

    type WorkplaceActorMessage = 
        | Reupload of WorkplaceError seq
        | Start