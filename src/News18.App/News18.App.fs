module News18.App
open Akkling
open News18.Reviews
open Google.Apis.Drive.v3
open System.Security.Cryptography.X509Certificates
open Google.Apis.Auth.OAuth2
open Google.Apis.Services
open Serilog
open Argu
open News18.Workplace.Actor
open News18.Workplace.Messages
open News18.Workplace.Models

type AppSettings = 
    | GoogleDriveAppName of name: string
    | GoogleDriveSecretPath of path: string
    | GoogleDriveServiceEmail of email: string
    | VirtualRossnetPath of path: string
    | PhysicalAttachmentsPath of path: string
    | FFmpegPath of path: string
    | WorkplaceAccessToken of accessToken: string
    | PortalDomain of portalDomain: string
    | TmpVideoDir of tmpVideoDir: string
    | [<NoCommandLine>] SiteRossnetSqlWriteServer of conn: string
    | Workplace
    | Attachments 
with 
    interface IArgParserTemplate with
        member x.Usage = 
            match x with
            | GoogleDriveAppName _ -> "Nazwa aplikacji google drive"
            | GoogleDriveSecretPath _ -> "Scieżka bezwgledna dla pliku z certyfikatem 'Service Account' google drive"
            | GoogleDriveServiceEmail _ -> "Email konta usługi"
            | VirtualRossnetPath _ -> "Wzgledna sciezka dla multmediow tak aby mozna ja bylo uzyc w aplikacji rossmann"
            | PhysicalAttachmentsPath _ -> "Fizyczna sciezka do zasobu, w ktorym maja byc skladowane zalaczniki"
            | SiteRossnetSqlWriteServer _ -> "Connection string do bazy rossnet"
            | FFmpegPath _ -> "Sciezka do exeka ffmpg"
            | WorkplaceAccessToken _ -> "Access token dla workplace"
            | PortalDomain _ -> "Adres domeny rossmann dla danego srodowiska"
            | TmpVideoDir _ -> "Katalog tymczasowy do zapisu plików video"
            | Workplace -> "Czy maszynka jest uruchomiona w trybie workplace"
            | Attachments -> "Czy maszynka jest uruchomina w trybie Attachments"

let googleDriveService(googleDriveSecretPath: string)(googleDriveServiceEmail: string)(googleDriveAppName: string) = 
    let scopes = [| DriveService.Scope.Drive; DriveService.Scope.DriveAppdata |]
    let certificate = new X509Certificate2(googleDriveSecretPath, "notasecret", X509KeyStorageFlags.MachineKeySet ||| X509KeyStorageFlags.PersistKeySet ||| X509KeyStorageFlags.Exportable)
    let initializer = ServiceAccountCredential.Initializer(googleDriveServiceEmail, Scopes = scopes)
    let serviceAccountCredentials = ServiceAccountCredential(initializer.FromCertificate(certificate))
    new DriveService(BaseClientService.Initializer(HttpClientInitializer = serviceAccountCredentials, ApplicationName = googleDriveAppName))

let logger = LoggerConfiguration()
                .WriteTo.NLog()
                .MinimumLevel.Information()
                .WriteTo.ColoredConsole()
                .MinimumLevel.Information()
                .Enrich.WithThreadId()
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .CreateLogger()

Serilog.Log.Logger <- logger

[<EntryPoint>]
let main _ =
    async {
        let parser = ArgumentParser.Create<AppSettings>(programName = "News18.App.exe")
        let result = parser.Parse()
        Serilog.Log.Logger.Information(sprintf "Start App With %A" result) 
        let system = System.create "rossmann" <| (Configuration.load())
        let driveService = googleDriveService(result.GetResult GoogleDriveSecretPath)(result.GetResult GoogleDriveServiceEmail)(result.GetResult GoogleDriveAppName)
        if result.Contains Workplace then 
            let workplaceActor = spawn system "workplace" <| props(workplaceActor(News18.Workplace.Database.db (result.GetResult SiteRossnetSqlWriteServer))({ WorkplaceBotAccessToken = result.GetResult WorkplaceAccessToken; PortalDomain = result.GetResult PortalDomain; TmpVideoPath = result.GetResult TmpVideoDir; FFmpegPath = result.GetResult FFmpegPath; DriveService = driveService  }))
            let! actorResponse = workplaceActor <? Start
            printfn "%A" actorResponse
        else 
            printfn "Elo"
        // do! News18.Reviews.Actor.run (system)(News18.Reviews.Database.db (result.GetResult SiteRossnetSqlWriteServer))(driveService)({ virtualPath = result.GetResult VirtualRossnetPath; physicalPath = result.GetResult PhysicalAttachmentsPath })
        return ()
    } |> Async.RunSynchronously
    0 // return an integer exit code

